# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

job "example" {
  datacenters = ["dc1"]
  type        = "service"

  group "example" {
    network {
#      mode = "driver"
      port "http" {
        to = 8080
      }
    }

    task "hello-world" {
      driver = "hello-world-example"

      config {
        unikernel = "/tmp/hello-key.hvt"
        # network "device1" {
        #   ports = ["http", ...]
        #   ip_arg = "--monitoring"
        #   gateway_arg = "--xxx"
        # }
        # args = [...]
      }
    }

    service {
      port = "http"
      name = "hello-service"
    }
  }
}
